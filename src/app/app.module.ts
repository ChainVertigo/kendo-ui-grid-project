import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { DropDownListModule } from '@progress/kendo-angular-dropdowns';
import { DialogModule } from '@progress/kendo-angular-dialog';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { PopupModule } from '@progress/kendo-angular-popup';
import { GridModule } from '@progress/kendo-angular-grid';

import { ProductsService } from './grid/products.service';
import { GridComponent } from './grid/grid.component';
import { AppComponent } from './app.component';

import { PopupAnchorDirective } from './grid/popup.anchor-target.directive';

@NgModule({
   declarations: [
      AppComponent,
      GridComponent,
      PopupAnchorDirective
   ],
   imports: [
      BrowserAnimationsModule,
      ReactiveFormsModule,
      DropDownListModule,
      BrowserModule,
      DialogModule,
      InputsModule,
      PopupModule,
      GridModule
   ],
   providers: [ProductsService],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
