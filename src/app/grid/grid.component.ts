import { PageSizeItem, SelectAllCheckboxState, GridDataResult } from '@progress/kendo-angular-grid';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit} from '@angular/core';

import { ProductsService } from './products.service';

import { categories } from './categories';
import { products } from './products';
import { Product } from './model';

const createFormGroup = dataItem => new FormGroup({
    'ProductID': new FormControl(dataItem.ProductID),
    'ProductName': new FormControl(dataItem.ProductName, Validators.required),
    'UnitPrice': new FormControl(dataItem.UnitPrice),
    'UnitsInStock': new FormControl(dataItem.UnitsInStock, Validators.compose([Validators.required, Validators.pattern('^[0-9]{1,3}')])),
    'CategoryID': new FormControl(dataItem.CategoryID, Validators.required)
});

@Component({
    selector: 'app-grid',
    templateUrl: './grid.component.html',
    styleUrls: ['./grid.component.css']

})
export class GridComponent implements OnInit{
    public buttonCount = 5;
    public info = true;
    public previousNext = true;
    public type: 'numeric' | 'input' = 'numeric';
    public categories: any[] = categories;
    public formGroup: FormGroup;
    private editedRowIndex: number;

    public gridData: GridDataResult;
    public data: any[] = products;

    public pageSize = 10;
    public skip = 0;
    public isNew: boolean;
    public pageSizes: PageSizeItem[] = [{ text: '10', value: 10 }, { text: '25', value: 25 }, { text: '100', value: 100 }, {
        text: 'All',
        value: 'all'
    }];
    public mySelection: number[] = [];
    public selectAllState: SelectAllCheckboxState = 'unchecked';
    constructor(private service: ProductsService) {
    }


    public onPageChange(state: any): void {
        this.pageSize = state.take;
        this.skip = state.skip;
        this.loadProducts();
    }

    private loadProducts(): void {
        this.gridData = {
            data: this.data.slice(this.skip, this.skip + this.pageSize),
            total: this.data.length
        };
    }

    public ngOnInit(): void {
        this.loadProducts();
    }

    public onSelectedKeysChange(e) {
        const len = this.mySelection.length;

        if (len === 0) {
            this.selectAllState = 'unchecked';
        } else if (len > 0 && len < this.data.length) {
            this.selectAllState = 'indeterminate';
        } else {
            this.selectAllState = 'checked';
        }
    }
    createNewProduct(): Product {
        return new Product();
    }

    public category(id: number): any {
        return this.categories.find(x => x.CategoryID === id);
    }


    public addHandler({ sender }) {
        this.closeEditor(sender);

        this.formGroup = createFormGroup({
            'ProductName': '',
            'UnitPrice': 0,
            'UnitsInStock': '',
            'CategoryID': 1
        });

        this.isNew = true;

        sender.addRow(this.formGroup);
    }


    public editHandler({ sender, rowIndex, dataItem }) {
        this.closeEditor(sender);

        this.formGroup = createFormGroup(dataItem);

        this.editedRowIndex = rowIndex;

        this.isNew = false;

        sender.editRow(rowIndex, this.formGroup);
    }


    public cancelHandler({ sender, rowIndex }) {
        this.closeEditor(sender, rowIndex);
        this.isNew = null;
    }


    public saveHandler({ sender, rowIndex, formGroup, isNew }): void {
        const product = formGroup.value;

        this.service.save(product, isNew);

        this.isNew = null;

        sender.closeRow(rowIndex);
    }

    public removeHandler({ dataItem }): void {
        this.service.remove(dataItem);
        this.loadProducts();
    }

    private closeEditor(grid, rowIndex = this.editedRowIndex) {
        grid.closeRow(rowIndex);
        this.editedRowIndex = undefined;
        this.formGroup = undefined;
    }

    public customSaveChanges(gridRef) {
        this.saveHandler({ sender: gridRef, rowIndex: this.editedRowIndex, formGroup: this.formGroup, isNew: this.isNew });
        this.loadProducts();
    }

    public addCustomItem(sender) {
        this.addHandler({ sender });
    }
}
