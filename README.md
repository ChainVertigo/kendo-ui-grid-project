You can find more information about the type of selection you want to achieve here. 
https://www.telerik.com/kendo-angular-ui/components/grid/selection/ 

You can find more information about the custom page size you want to achieve here. 
https://www.telerik.com/kendo-angular-ui/components/grid/paging/

You can find more information about editing the grid here.
https://www.telerik.com/kendo-angular-ui/components/grid/editing/editing-reactive-forms/

For the custom buttons outside the grid I have created a service and two custom functions addCustomItem and saveItemChanges

